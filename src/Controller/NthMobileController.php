<?php

namespace Drupal\nth_mobile\Controller;

use Drupal\Core\Controller\ControllerBase;
use Drupal\Core\Entity\ContentEntityType;
use Drupal\Core\Url;

/**
 * Returns responses for nth_mobile module routes.
 */
class NthMobileController extends ControllerBase {

  /**
   * Show entity types.
   *
   * @return array
   *   Array of page elements to render.
   */
  public function entityTypeList() {
    $output = [
      '#theme' => 'admin_block_content',
      '#content' => [],
    ];

    foreach ($this->getEntityTypes() as $key => $entityType) {
      $output['#content'][] = [
        'url' => Url::fromRoute('entity.' . $key . '.settings'),
        'title' => $entityType->getLabel(),
      ];
    }

    return $output;
  }

  /**
   * Get list of available EntityTypes.
   *
   * @return \Drupal\Core\Entity\ContentEntityTypeInterface[]
   *   List of content entity types.
   */
  private function getEntityTypes() {
    $entityTypes = [];

    foreach ($this->entityTypeManager()->getDefinitions() as $key => $entityType) {
      if ($entityType instanceof ContentEntityType) {
        // Filter for NTH mobile entity types.
        if (substr($key, 0, 11) == 'nth_mobile_') {
          $entityTypes[$key] = $entityType;
        }
      }
    }

    return $entityTypes;
  }

}
