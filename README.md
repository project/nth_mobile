# NTH Mobile

The NTH Mobile module enables payments in Drupal through NTH Mobile. The module
provides events that other modules can subscribe to and build their business
logic around.

* For a full description of the module, visit the project page:
  https://www.drupal.org/project/nth_mobile

* To submit bug reports and feature suggestions, or track changes:
  https://www.drupal.org/project/issues/nth_mobile

## REQUIREMENTS

This module requires you to have an account with [NTH Mobile](https://www.nth-mobile.com).

## INSTALLATION

The recommended way to install this module is
[using Composer](https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-composer-dependencies).

`composer require drupal/nth_mobile`

Alternatively you can install the module from
[the command line using Drush or Drupal Console](https://www.drupal.org/docs/8/extending-drupal-8/installing-modules-from-the-command-line)
or [manually](https://www.drupal.org/docs/8/extending-drupal-8/installing-drupal-8-modules).


## CONFIGURATION

* Configure the user permissions in Administration » People » Permissions:

  - Administer NTH Mobile

    The top-level administration menu item require this permission to be
    accessible. The administrator will not be able to change any configuration
    when this permission is not given.

* Get an overview of the settings in Administration » Configuration » Web
  services » NTH Mobile.

## MAINTAINERS

Current maintainers:
* Tim Diels (tim-diels) - https://www.drupal.org/u/2915097

This project has been sponsored by:
* Decency - https://www.decency.be
