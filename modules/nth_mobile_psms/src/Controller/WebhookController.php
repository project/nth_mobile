<?php

namespace Drupal\nth_mobile_psms\Controller;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Controller\ControllerBase;
use Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverEventNotificationEvent;
use Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverMessageNotificationEvent;
use Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverReportNotificationEvent;
use Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\EventDispatcher\EventDispatcherInterface;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\HttpFoundation\Response;

/**
 * Returns responses for NTH Mobile routes.
 */
class WebhookController extends ControllerBase {

  /**
   * The NTH Mobile Premium SMS service.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface
   */
  protected $nthMobilePsms;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * Event dispatcher.
   *
   * @var \Symfony\Component\EventDispatcher\EventDispatcherInterface
   */
  protected $eventDispatcher;

  /**
   * WebhookController constructor.
   *
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface $nth_mobile_psms
   *   The NTH Mobile Premium SMS service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Symfony\Component\EventDispatcher\EventDispatcherInterface $event_dispatcher
   *   Event dispatcher.
   */
  public function __construct(NthMobilePsmsInterface $nth_mobile_psms, RequestStack $request_stack, EventDispatcherInterface $event_dispatcher) {
    $this->nthMobilePsms = $nth_mobile_psms;
    $this->requestStack = $request_stack;
    $this->eventDispatcher = $event_dispatcher;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('nth_mobile_psms'),
      $container->get('request_stack'),
      $container->get('event_dispatcher')
    );
  }

  /**
   * Checks access for a specific request.
   *
   * @return \Drupal\Core\Access\AccessResultInterface
   *   The access result.
   */
  public function access() {
    if ($this->nthMobilePsms->bypassEnabled()) {
      return AccessResult::allowed();
    }
    else {
      if (empty($this->nthMobilePsms->getIpWhitelist()) || $this->nthMobilePsms->isWhitelistedIp()) {
        if ($this->checkShortcode()) {
          return AccessResult::allowed();
        }
        else {
          return AccessResult::forbidden('Provided shortcode is not correct.');
        }
      }
      else {
        return AccessResult::forbidden('The IP ' . $this->requestStack->getCurrentRequest()->getClientIp() . ' is not whitelisted.');
      }
    }
  }

  /**
   * Check if the businessNumber is the same as the configured shortcode.
   *
   * @return bool
   *   True if the shortcode matches the businessNumber, false otherwise.
   */
  protected function checkShortcode() {
    $businessNumber = $this->requestStack->getCurrentRequest()->get('businessNumber');
    $shortcode = $this->nthMobilePsms->getConfig()->get('shortcode');

    if (intval($businessNumber) === $shortcode) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * Allows modules to react to NTH Mobile Premium SMS deliverMessage events.
   *
   * This webhook is called by NTH Mobile.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response that will inform NTH Mobile whether the event was processed
   *   correctly.
   */
  public function invokeDeliverMessageHook(): Response {
    $context = 'premium_sms';
    $params = $this->getParameters('deliverMessage');

    // Create an event to dispatch notification handled by appropriate modules.
    $event = new NthMobilePsmsDeliverMessageNotificationEvent(
      $context,
      $params['messageId'],
      $params
    );

    // Set the default HTTP code.
    $event->setHttpStatusCode(200);

    // Dispatch event.
    $this->eventDispatcher->dispatch(
      $event,
      NthMobilePsmsDeliverMessageNotificationEvent::EVENT_NAME
    );

    return new Response('', $event->getHttpStatusCode());
  }

  /**
   * Allows modules to react to NTH Mobile Premium SMS deliverReport events.
   *
   * This webhook is called by NTH Mobile.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response that will inform NTH Mobile whether the event was processed
   *   correctly.
   */
  public function invokeDeliverReportHook(): Response {
    $context = 'premium_sms';
    $params = $this->getParameters('deliverReport');

    // Create an event to dispatch notification handled by appropriate modules.
    $event = new NthMobilePsmsDeliverReportNotificationEvent(
      $context,
      $params['messageId'],
      $params
    );

    // Set the default HTTP code.
    $event->setHttpStatusCode(200);

    // Dispatch event.
    $this->eventDispatcher->dispatch(
      $event,
      NthMobilePsmsDeliverReportNotificationEvent::EVENT_NAME
    );

    return new Response('', $event->getHttpStatusCode());
  }

  /**
   * Allows modules to react to NTH Mobile Premium SMS deliverEvent events.
   *
   * This webhook is called by NTH Mobile.
   *
   * @return \Symfony\Component\HttpFoundation\Response
   *   Response that will inform NTH Mobile whether the event was processed
   *   correctly.
   */
  public function invokeDeliverEventHook(): Response {
    $context = 'premium_sms';
    $params = $this->getParameters('deliverEvent');

    // Create an event to dispatch notification handled by appropriate modules.
    $event = new NthMobilePsmsDeliverEventNotificationEvent(
      $context,
      $params['messageId'],
      $params,
      $params['event']
    );

    // Set the default HTTP code.
    $event->setHttpStatusCode(200);

    // Dispatch event.
    $this->eventDispatcher->dispatch(
      $event,
      NthMobilePsmsDeliverEventNotificationEvent::EVENT_NAME
    );

    return new Response('', $event->getHttpStatusCode());
  }

  /**
   * Get the parameters from the request per type.
   *
   * @param string $type
   *   The type of request.
   *
   * @return string[]
   *   The parameters.
   */
  protected function getParameters(string $type) {
    $params = [
      'command' => $this->requestStack->getCurrentRequest()->get('command'),
      'messageId' => $this->requestStack->getCurrentRequest()->get('messageId'),
      'msisdn' => $this->requestStack->getCurrentRequest()->get('msisdn'),
      'businessNumber' => $this->requestStack->getCurrentRequest()->get('businessNumber'),
      'time' => $this->requestStack->getCurrentRequest()->get('time'),
      'sessionId' => $this->requestStack->getCurrentRequest()->get('sessionId'),

    ];
    switch ($type) {
      case 'deliverMessage':
        $params += [
          'operatorCode' => $this->requestStack->getCurrentRequest()->get('operatorCode'),
          'keyword' => $this->requestStack->getCurrentRequest()->get('keyword'),
          'content' => $this->requestStack->getCurrentRequest()->get('content'),
        ];
        break;

      case 'deliverReport':
        $params += [
          'messageRef' => $this->requestStack->getCurrentRequest()->get('messageRef'),
          'messageStatus' => $this->requestStack->getCurrentRequest()->get('messageStatus'),
          'messageStatusText' => $this->requestStack->getCurrentRequest()->get('messageStatusText'),
        ];
        break;

      case 'deliverEvent':
        $params += [
          'event' => $this->requestStack->getCurrentRequest()->get('event'),
          'operatorCode' => $this->requestStack->getCurrentRequest()->get('operatorCode'),
          'keyword' => $this->requestStack->getCurrentRequest()->get('keyword'),
          'price' => $this->requestStack->getCurrentRequest()->get('price'),
          'content' => $this->requestStack->getCurrentRequest()->get('content'),
        ];
        break;
    }
    return $params;
  }

}
