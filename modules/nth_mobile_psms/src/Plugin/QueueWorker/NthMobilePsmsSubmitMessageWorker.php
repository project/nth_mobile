<?php

namespace Drupal\nth_mobile_psms\Plugin\QueueWorker;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Core\Database\Connection;
use Drupal\Core\Datetime\DrupalDateTime;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Drupal\Core\Queue\QueueWorkerBase;
use Drupal\nth_mobile_psms\Service\NthMobilePsmsApiInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Sends MT Messages that were not send for an unknown reason.
 *
 * @QueueWorker(
 *   id = "nth_mobile_psms_submit_message_worker",
 *   title = @Translation("NTH Mobile Premium SMS submitMessage worker"),
 * )
 */
class NthMobilePsmsSubmitMessageWorker extends QueueWorkerBase implements ContainerFactoryPluginInterface {

  /**
   * The database.
   *
   * @var \Drupal\Core\Database\Connection
   */
  protected $database;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * A date time instance.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The NTH Mobile Premium SMS API service.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsApiInterface
   */
  protected $nthMobilePsmsApi;

  /**
   * Constructs a new NthMobilePsmsSubmitMessageWorker instance.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin_id for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Database\Connection $database
   *   The database service.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entityTypeManager
   *   The entity type manager.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   A date time instance.
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsApiInterface $nthMobilePsmsApi
   *   The NTH Mobile Premium SMS API service.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Connection $database, EntityTypeManagerInterface $entityTypeManager, TimeInterface $time, NthMobilePsmsApiInterface $nthMobilePsmsApi) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->database = $database;
    $this->entityTypeManager = $entityTypeManager;
    $this->time = $time;
    $this->nthMobilePsmsApi = $nthMobilePsmsApi;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('database'),
      $container->get('entity_type.manager'),
      $container->get('datetime.time'),
      $container->get('nth_mobile_psms.api')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function processItem($data) {
    /** @var \Drupal\nth_mobile_psms\Entity\NthMobilePsmsTransaction $entity */
    $entity = $this->entityTypeManager
      ->getStorage('nth_mobile_psms_transaction')
      ->load($data->retryMessageRef);

    // Only retry sending messages within 24 hours.
    $twenty_four_hours = DrupalDateTime::createFromTimestamp($entity->getCreatedTime())
      ->modify('-24 hours')
      ->getTimestamp();
    if ($this->time->getCurrentTime() <= $twenty_four_hours) {

      // Try sending the message.
      $result = $this->nthMobilePsmsApi->submitMessage(
        $data->content,
        $data->data,
        $data->price,
        $data->userId,
        $data->language,
        $data->retryMessageRef,
      );

      if (!$this->nthMobilePsmsApi->processResultCode($result)) {
        throw new \Exception('Premium SMS could not be send.');
      }
    }
  }

}
