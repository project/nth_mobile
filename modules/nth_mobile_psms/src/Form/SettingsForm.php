<?php

namespace Drupal\nth_mobile_psms\Form;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Form\ConfigFormBase;
use Drupal\Core\Form\FormStateInterface;
use Drupal\nth_mobile_psms\Service\NthMobilePsmsConfigValidatorInterface;
use Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;

/**
 * Configure NTH Mobile Premium SMS settings for this site.
 */
class SettingsForm extends ConfigFormBase {

  /**
   * The NTH Mobile Premium SMS service.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface
   */
  protected $nthMobilePsms;

  /**
   * The NTH Mobile Premium SMS config validator.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsConfigValidatorInterface
   */
  protected $nthMobilePsmsConfigValidator;

  /**
   * Constructs a SettingsForm object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   The factory for configuration objects.
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface $nth_mobile_psms
   *   The NTH Mobile Premium SMS service.
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsConfigValidatorInterface $nth_mobile_psms_config_validator
   *   The NTH Mobile Premium SMS config validator.
   */
  public function __construct(ConfigFactoryInterface $config_factory, NthMobilePsmsInterface $nth_mobile_psms, NthMobilePsmsConfigValidatorInterface $nth_mobile_psms_config_validator) {
    parent::__construct($config_factory);
    $this->nthMobilePsms = $nth_mobile_psms;
    $this->nthMobilePsmsConfigValidator = $nth_mobile_psms_config_validator;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('config.factory'),
      $container->get('nth_mobile_psms'),
      $container->get('nth_mobile_psms.config_validator')
    );
  }

  /**
   * {@inheritdoc}
   */
  public function getFormId() {
    return 'nth_mobile_psms_settings';
  }

  /**
   * {@inheritdoc}
   */
  protected function getEditableConfigNames() {
    return ['nth_mobile_psms.settings'];
  }

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state) {
    $config = $this->config('nth_mobile_psms.settings');

    // Check username and password.
    $checkMarkup = $this->generateCheckMarkup(
        $this->nthMobilePsmsConfigValidator->hasUsername(),
        'Username'
      ) . '<br>';
    $checkMarkup .= $this->generateCheckMarkup(
        $this->nthMobilePsmsConfigValidator->hasPassword(),
        'Password'
      );

    $form['account'] = [
      '#type' => 'details',
      '#open' => TRUE,
      '#title' => $this->t('Customer account'),
      'check' => [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $checkMarkup,
      ],
    ];

    if ($this->nthMobilePsmsConfigValidator->hasUsername() && $this->nthMobilePsmsConfigValidator->hasPassword()) {
      $form['account']['shortcode'] = [
        '#type' => 'number',
        '#title' => $this->t('Shortcode'),
        '#description' => $this->t('Business number to which messages need to be sent.'),
        '#default_value' => $config->get('shortcode'),
        '#required' => TRUE,
        '#min' => 0,
        '#step' => 1,
      ];

      $form['account']['keyword'] = [
        '#type' => 'textfield',
        '#title' => $this->t('Keyword'),
        '#description' => $this->t('Service keyword; appropriate for routing purposes on Customer’s end if a single application receives messages for several different services.'),
        '#default_value' => $config->get('keyword'),
        '#required' => TRUE,
      ];

      $form['mt'] = [
        '#type' => 'details',
        '#title' => $this->t('MT Messages'),
        '#open' => TRUE,
      ];

      $form['mt']['mt_submission_url'] = [
        '#type' => 'url',
        '#title' => $this->t('Submission URL'),
        '#description' => $this->t('The URL to which HTTP request with MT message should be send.'),
        '#default_value' => $config->get('mt_submission_url'),
        '#required' => TRUE,
      ];

      $form['mt']['mt_nwc_operators'] = [
        '#type' => 'textarea',
        '#title' => $this->t('Operators'),
        '#default_value' => $config->get('mt_nwc_operators'),
        '#description' => $this->t('Specify MT operators by their NWC. One code per line.'),
      ];

      $form['access'] = [
        '#type' => 'details',
        '#title' => $this->t('Access'),
      ];

      $form['access']['access_trusted_ips'] = [
        '#type' => 'textarea',
        '#title' => $this->t('IPv4 addresses or ranges that must bypass access check'),
        '#default_value' => $config->get('access_trusted_ips'),
        '#description' => $this->t('Allows to bypass access restrictions. One IP address per line.'),
      ];

      $form['access']['access_bypass'] = [
        '#type' => 'checkbox',
        '#title' => $this->t('Bypass IP restriction'),
        '#default_value' => $config->get('access_bypass'),
      ];
    }
    else {
      $form['instruction'] = [
        '#type' => 'html_tag',
        '#tag' => 'p',
        '#value' => $this->t('You need to configure the username and password in the settings. Please check the readme for more information.'),
      ];
    }

    return parent::buildForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function validateForm(array &$form, FormStateInterface $form_state) {

    // Only validate form when we have values to validate.
    if ($this->nthMobilePsmsConfigValidator->hasUsername() && $this->nthMobilePsmsConfigValidator->hasPassword()) {

      // Validate shortcode.
      if (strlen($form_state->getValue('shortcode')) < 4 || strlen($form_state->getValue('shortcode')) > 20) {
        $form_state->setErrorByName('shortcode', $this->t('The shortcode must be between 4 and 20 digits.'));
      }

      // Validate IP whitelist.
      $ip_whitelist_string = $form_state->getValue('access_trusted_ips');
      $ip_whitelist_array = $this->nthMobilePsms->configTextToArray($ip_whitelist_string);

      $errors_ipv4 = [];
      $errors_ipv4_range = [];
      foreach ($ip_whitelist_array as $index => $ip) {

        // Strip leading/trailing spaces as we go.
        $ip_whitelist_array[$index] = $ip = trim($ip);

        // Make sure the IP address and netmask of a range value are valid.
        // We don't check if the range is actually possible, for now.
        if (strpos($ip, '/')) {

          [$address, $netmask] = explode('/', $ip, 2);
          if (!filter_var($address, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)
            || $netmask < 0 || $netmask > 32
          ) {
            $errors_ipv4_range[] = $ip;
          }
        }
        else {
          // Make sure IP addresses are valid.
          if (!filter_var($ip, FILTER_VALIDATE_IP, FILTER_FLAG_IPV4)) {
            $errors_ipv4[] = $ip;
          }
        }
      }

      $form_state->setValue('access_trusted_ips', $this->nthMobilePsms->configArrayToText($ip_whitelist_array));

      // Process whitelist errors.
      $messages = [];
      if ($errors_ipv4) {
        $messages[] = $this->formatPlural(count($errors_ipv4), "@ip is not a valid IPv4 address.", "@ip are not valid IPv4 addresses.", ['@ip' => implode(', ', $errors_ipv4)]);
      }

      if ($errors_ipv4_range) {
        $messages[] = $this->formatPlural(count($errors_ipv4_range), "@ip is not a valid IPv4 range.", "@ip are not valid IPv4 ranges.", ['@ip' => implode(', ', $errors_ipv4_range)]);
      }

      if (!empty($messages)) {
        $form_state->setError($form['access_trusted_ips'], implode(' ', $messages));
      }
    }

    parent::validateForm($form, $form_state);
  }

  /**
   * {@inheritdoc}
   */
  public function submitForm(array &$form, FormStateInterface $form_state) {
    $this->config('nth_mobile_psms.settings')
      ->set('shortcode', $form_state->getValue('shortcode'))
      ->set('keyword', $form_state->getValue('keyword'))
      ->set('mt_submission_url', $form_state->getValue('mt_submission_url'))
      ->set('mt_nwc_operators', $form_state->getValue('mt_nwc_operators'))
      ->set('access_trusted_ips', $form_state->getValue('access_trusted_ips'))
      ->set('access_bypass', $form_state->getValue('access_bypass'))
      ->save();

    parent::submitForm($form, $form_state);
  }

  /**
   * Returns markup for a config check.
   *
   * @param bool $hasSetting
   *   True if the config exists, false otherwise.
   * @param string $title
   *   Title to display for the config.
   *
   * @return string
   *   Markup for the config check.
   */
  protected function generateCheckMarkup(bool $hasSetting, string $title): string {
    $markup = $hasSetting ? '&#10003;&nbsp;' : '&#10007;&nbsp;';

    if ($hasSetting) {
      $markup .= $this->t(
        '@title is configured.',
        ['@title' => $title]
      );
    }
    else {
      $markup .= $this->t(
        '@title is not configured.',
        ['@title' => $title]
      );
    }

    return $markup;
  }

}
