<?php

namespace Drupal\nth_mobile_psms\Form;

use Drupal\Core\Entity\ContentEntityForm;
use Drupal\Core\Form\FormStateInterface;

/**
 * Form controller for the PSMS Transaction entity edit forms.
 */
class NthMobilePsmsTransactionForm extends ContentEntityForm {

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state) {

    $entity = $this->getEntity();
    $result = $entity->save();
    $link = $entity->toLink($this->t('View'))->toRenderable();

    $message_arguments = ['%label' => $this->entity->label()];
    $logger_arguments = $message_arguments + ['link' => render($link)];

    if ($result == SAVED_NEW) {
      $this->messenger()->addStatus($this->t('New PSMS Transaction %label has been created.', $message_arguments));
      $this->logger('nth_mobile_psms')->notice('Created new PSMS Transaction %label', $logger_arguments);
    }
    else {
      $this->messenger()->addStatus($this->t('The PSMS Transaction %label has been updated.', $message_arguments));
      $this->logger('nth_mobile_psms')->notice('Updated new PSMS Transaction %label.', $logger_arguments);
    }

    $form_state->setRedirect('entity.nth_mobile_psms_transaction.canonical', ['nth_mobile_psms_transaction' => $entity->id()]);
  }

}
