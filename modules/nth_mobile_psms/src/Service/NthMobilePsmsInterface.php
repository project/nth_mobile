<?php

namespace Drupal\nth_mobile_psms\Service;

use Symfony\Component\HttpFoundation\Request;

/**
 * Interface NthMobilePsmsInterface.
 *
 * Describes the NthMobilePsms service functions.
 */
interface NthMobilePsmsInterface {

  /**
   * Returns the module config.
   *
   * @return \Drupal\Core\Config\ImmutableConfig
   *   The config.
   */
  public function getConfig();

  /**
   * Get the NTH mobile Premium SMS logger channel.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger();

  /**
   * Get the MT Operators.
   *
   * @return string[]
   *   The MT Operators.
   */
  public function getMtOperators();

  /**
   * Check NWC is MT Operator.
   *
   * @param string $nwc
   *   The NWC of the operator.
   *
   * @return bool
   *   True if NWC is MT Operator, false otherwise.
   */
  public function isMtOperator(string $nwc);

  /**
   * Get the IPv4 list.
   *
   * @return string[]
   *   The IP's.
   */
  public function getIpWhitelist();

  /**
   * Check client IP(s) is (are) whitelisted for access bypass.
   *
   * @param \Symfony\Component\HttpFoundation\Request|null $request
   *   The request to check. Defaults to current request.
   *
   * @return bool
   *   True if IP(s) is (are) whitelisted, false otherwise.
   */
  public function isWhitelistedIp(Request $request = NULL);

  /**
   * Check if the bypass is enabled.
   *
   * @return bool
   *   True if a bypass is enabled, false otherwise.
   */
  public function bypassEnabled();

  /**
   * Convert a config string (aka 'one item per line') to an array.
   *
   * @param string $string
   *   The config as string.
   *
   * @return array
   *   The config as array.
   */
  public function configTextToArray(string $string);

  /**
   * Convert a config array to a 'one item per line' string.
   *
   * @param array $array
   *   The config as array.
   *
   * @return string
   *   The config as string.
   */
  public function configArrayToText(array $array);

  /**
   * Add an item to the queue.
   *
   * You can use drush to process the queue.
   * "queue:run nth_mobile_psms_submit_message_worker".
   *
   * @param object $data
   *   The data to save to the queue.
   */
  public function addToQueue(\stdClass $data);

}
