<?php

namespace Drupal\nth_mobile_psms\Service;

/**
 * Interface NthMobilePsmsApiInterface.
 *
 * Describes the NthMobilePsmsApi service functions.
 */
interface NthMobilePsmsApiInterface {

  /**
   * Issue submitMessage request to submit MT SMS message to end user.
   *
   * @param string $content
   *   The SMS message text.
   * @param array $data
   *   The data provided by the MO message.
   * @param int $price
   *   The message price in money units.
   * @param int $userId
   *   The user ID.
   * @param string|null $language
   *   The user's language, in two-letter ISO 639-1 format.
   * @param string|null $messageRef
   *   The message ref.
   * @param string|null $label
   *   The purpose of the message.
   */
  public function submitMessage(string $content, array $data, int $price, int $userId, string $language = NULL, string $messageRef = NULL, string $label = NULL);

  /**
   * Process the resultCode returned by NTH Mobile after sending a message.
   *
   * @param array $result
   *   The result returned by NTH Mobile.
   *
   * @return bool
   *   True if message is sent, false otherwise.
   */
  public function processResultCode(array $result);

}
