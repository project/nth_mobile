<?php

namespace Drupal\nth_mobile_psms\Service;

/**
 * Interface NthMobilePsmsConfigValidatorInterface.
 *
 * The NthMobilePsmsConfigValidator interface.
 */
interface NthMobilePsmsConfigValidatorInterface {

  /**
   * Determines whether a username is configured.
   *
   * @return bool
   *   True if a username is configured, false otherwise.
   */
  public function hasUsername(): bool;

  /**
   * Determines whether a password is configured.
   *
   * @return bool
   *   True if a password is configured, false otherwise.
   */
  public function hasPassword(): bool;

}
