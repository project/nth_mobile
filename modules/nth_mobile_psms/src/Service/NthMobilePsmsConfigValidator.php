<?php

namespace Drupal\nth_mobile_psms\Service;

use Drupal\Core\Site\Settings;

/**
 * Class NthMobilePsmsConfigValidator.
 *
 * The NthMobilePsmsConfigValidator service.
 */
class NthMobilePsmsConfigValidator implements NthMobilePsmsConfigValidatorInterface {

  /**
   * {@inheritdoc}
   */
  public function hasUsername(): bool {
    return $this->hasSetting('username');
  }

  /**
   * {@inheritdoc}
   */
  public function hasPassword(): bool {
    return $this->hasSetting('password');
  }

  /**
   * Determines whether a certain NTH Mobile Premium SMS setting is configured.
   *
   * @param string $name
   *   The name of the setting to check.
   *
   * @return bool
   *   True if the setting with the given name is set and not empty, false
   *   otherwise.
   */
  protected function hasSetting(string $name): bool {
    $nthMobilePsmsSettings = Settings::get('nth_mobile_psms.settings');

    return isset($nthMobilePsmsSettings[$name]) && !empty($nthMobilePsmsSettings[$name]);
  }

}
