<?php

namespace Drupal\nth_mobile_psms\Service;

use Drupal\Component\Datetime\TimeInterface;
use Drupal\Component\Render\FormattableMarkup;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Http\ClientFactory;
use Drupal\Core\Site\Settings;
use Drupal\nth_mobile_psms\Entity\NthMobilePsmsTransaction;
use GuzzleHttp\Exception\RequestException;
use Symfony\Component\HttpFoundation\RequestStack;
use Symfony\Component\Serializer\SerializerInterface;

/**
 * Class NthMobilePsmsApi.
 *
 * The NthMobilePsmsApi service.
 */
class NthMobilePsmsApi implements NthMobilePsmsApiInterface {

  /**
   * The NTH Mobile Premium SMS service.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface
   */
  protected $nthMobilePsms;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * The HTTP Client Factory.
   *
   * @var \Drupal\Core\Http\ClientFactory
   */
  protected $clientFactory;

  /**
   * The serializer.
   *
   * @var \Symfony\Component\Serializer\SerializerInterface
   */
  private $serializer;

  /**
   * The time interface.
   *
   * @var \Drupal\Component\Datetime\TimeInterface
   */
  protected $time;

  /**
   * The entity type manager.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * Constructs a NthMobilePsmsApi object.
   *
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface $nth_mobile_psms
   *   The NTH Mobile Premium SMS service.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Http\ClientFactory $client_factory
   *   The HTTP Client Factory.
   * @param \Symfony\Component\Serializer\SerializerInterface $serializer
   *   The serializer.
   * @param \Drupal\Component\Datetime\TimeInterface $time
   *   The time interface.
   * @param \Drupal\Core\Entity\EntityTypeManagerInterface $entity_type_manager
   *   The entity type manager.
   */
  public function __construct(NthMobilePsmsInterface $nth_mobile_psms, RequestStack $request_stack, ClientFactory $client_factory, SerializerInterface $serializer, TimeInterface $time, EntityTypeManagerInterface $entity_type_manager) {
    $this->nthMobilePsms = $nth_mobile_psms;
    $this->requestStack = $request_stack;
    $this->clientFactory = $client_factory;
    $this->serializer = $serializer;
    $this->time = $time;
    $this->entityTypeManager = $entity_type_manager;
  }

  /**
   * {@inheritdoc}
   */
  public function submitMessage($content, $data, $price, $userId, $language = NULL, $messageRef = NULL, $label = NULL) {
    $uri = $this->nthMobilePsms->getConfig()->get('mt_submission_url');

    // Required params.
    $params = [
      'command' => 'submitMessage',
      'username' => Settings::get('nth_mobile_psms.settings')['username'],
      'password' => Settings::get('nth_mobile_psms.settings')['password'],
      'msisdn' => $data['msisdn'],
      'businessNumber' => $data['businessNumber'],
      'content' => $content,
      'price' => $price,
      'sessionId' => $data['sessionId'],
    ];

    // Optional params.
    $params += [
      'keyword' => $data['keyword'],
      'operatorCode' => $data['operatorCode'],
    ];

    // Add language if specified.
    if (isset($language)) {
      $params += [
        'language' => $language,
      ];
    }

    // Add label if specified.
    if (isset($label)) {
      $params += [
        'label' => $label,
      ];
    }

    // Initiate a HTTP client with a timeout of 180 as requested in the
    // documentation.
    $client = $this->clientFactory->fromOptions(['timeout' => 180]);

    try {
      // Create PSMS transaction for further follow up.
      if (!isset($data['messageRef'])) {
        $values = [
          'title' => $data['content'],
          'status' => 1,
          'msisdn' => $data['msisdn'],
          'business_number' => $data['businessNumber'],
          'content' => $content,
          'price' => $price,
          'operator_code' => $data['operatorCode'],
          'message_state' => 'draft',
          'message_type' => $this->nthMobilePsms->isMtOperator($data['operatorCode']) ? 'mt' : 'mo',
          'uid' => $userId,
        ];

        $transaction = $this->entityTypeManager
          ->getStorage('nth_mobile_psms_transaction')
          ->create($values);

        $transaction->save();

        $params['messageRef'] = $transaction->id();
      }
      // Load existing PSMS transaction for further follow up.
      else {
        $transaction = $this->entityTypeManager
          ->getStorage('nth_mobile_psms_transaction')
          ->load($data['messageRef']);
      }

      // Send the message.
      $response = $client->post($uri, [
        'verify' => TRUE,
        'form_params' => $params,
        'headers' => [
          'Content-type' => 'application/x-www-form-urlencoded',
        ],
      ])->getBody()->getContents();

      $result = $this->serializer->decode($response, 'xml');

      // Update the PSMS transaction if result returns correct status.
      if (is_array($result) && $this->processResultCode($result)) {
        $transaction->set('session_id', $result['sessionId']);
        $transaction->set('message_id', $result['messageId']);
        $transaction->set('message_state', 'pending');

        // Add revision message.
        if ($transaction->getEntityType()->isRevisionable()) {
          $transaction->setRevisionCreationTime($this->time->getRequestTime());
          $transaction->setRevisionLogMessage('Transaction changed by system.');
          $transaction->setNewRevision();
        }

        $transaction->save();
      }
      // Add the messageRef for follow up.
      else {
        $result = [
          'result' => $result,
          'retryMessageRef' => $transaction->id(),
        ];
      }

      return $result;
    }
    catch (RequestException $e) {
      watchdog_exception('nth_mobile_psms', $e);
      return [];
    }
  }

  /**
   * {@inheritdoc}
   */
  public function processResultCode(array $result) {
    if (!isset($result['resultCode']) || $result['resultCode'] !== '100') {
      $this->nthMobilePsms->getLogger()->debug(new FormattableMarkup(
        'There was an error sending the Premium SMS message. Error details are as follows:<pre>@response</pre>',
        [
          '@response' => print_r($result, TRUE),
        ]
      ));

      return FALSE;
    }

    return TRUE;
  }

}
