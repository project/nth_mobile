<?php

namespace Drupal\nth_mobile_psms\Service;

use Drupal\Core\Config\ConfigFactoryInterface;
use Drupal\Core\Queue\QueueFactory;
use Psr\Log\LoggerInterface;
use Symfony\Component\HttpFoundation\IpUtils;
use Symfony\Component\HttpFoundation\Request;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Class NthMobilePsms.
 *
 * The NthMobilePsms service.
 */
class NthMobilePsms implements NthMobilePsmsInterface {

  /**
   * Configuration object.
   *
   * @var \Drupal\Core\Config\ImmutableConfig
   */
  protected $config;

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * MT Operators.
   *
   * @var string[]
   */
  protected static $mtOperators;

  /**
   * True if NWC is MT Operator, false otherwise.
   *
   * @var bool
   */
  protected $isMtOperator;

  /**
   * IPv4 list that must bypass access check.
   *
   * @var string[]
   */
  protected static $ipv4Whitelist;

  /**
   * TRUE if the client IP(s) is (are) whitelisted for access bypass.
   *
   * @var bool
   */
  protected $isWhitelistedIp;

  /**
   * The queue factory.
   *
   * @var \Drupal\Core\Queue\QueueFactory
   */
  protected $queueFactory;

  /**
   * Constructs a NthMobilePsms object.
   *
   * @param \Drupal\Core\Config\ConfigFactoryInterface $config_factory
   *   Configuration factory.
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   * @param \Drupal\Core\Queue\QueueFactory $queueFactory
   *   The queue factory.
   */
  public function __construct(ConfigFactoryInterface $config_factory, LoggerInterface $logger, RequestStack $request_stack, QueueFactory $queueFactory) {
    $this->config = $config_factory->get('nth_mobile_psms.settings');
    $this->logger = $logger;
    $this->requestStack = $request_stack;
    $this->queueFactory = $queueFactory;
  }

  /**
   * {@inheritdoc}
   */
  public function getConfig() {
    return $this->config;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger() {
    return $this->logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getMtOperators() {
    if (!static::$mtOperators) {
      static::$mtOperators = $this->configTextToArray($this->config->get('mt_nwc_operators'));
    }

    return static::$mtOperators;
  }

  /**
   * {@inheritdoc}
   */
  public function isMtOperator($nwc) {
    if (!$this->isMtOperator) {

      $this->isMtOperator = FALSE;

      $mtOperators = $this->getMtOperators();
      $this->isMtOperator = in_array($nwc, $mtOperators);
    }

    return $this->isMtOperator;
  }

  /**
   * {@inheritdoc}
   */
  public function getIpWhitelist() {
    if (!static::$ipv4Whitelist) {
      static::$ipv4Whitelist = $this->configTextToArray($this->config->get('access_trusted_ips'));
    }

    return static::$ipv4Whitelist;
  }

  /**
   * {@inheritdoc}
   */
  public function isWhitelistedIp(Request $request = NULL) {

    if (!$this->isWhitelistedIp) {

      $this->isWhitelistedIp = FALSE;

      // If no request is given, try to get from stack.
      if (!$request) {
        $request = $this->requestStack->getCurrentRequest();
      }

      if ($request) {
        $client_ip = $request->getClientIp();
        $whitelist = $this->getIpWhitelist();

        // Try direct match.
        $this->isWhitelistedIp = in_array($client_ip, $whitelist);

        // No match, then check ranges, if any.
        if (!$this->isWhitelistedIp) {
          foreach ($whitelist as $item) {

            if (strpos($item, '/') && IpUtils::checkIp($client_ip, $item)) {
              $this->isWhitelistedIp = TRUE;
              break;
            }
          }
        }
      }
    }

    return $this->isWhitelistedIp;
  }

  /**
   * {@inheritdoc}
   */
  public function bypassEnabled() {
    return $this->config->get('access_bypass');
  }

  /**
   * {@inheritdoc}
   */
  public function configTextToArray($string) {
    return ($string ? explode("\r\n", trim($string)) : []);
  }

  /**
   * {@inheritdoc}
   */
  public function configArrayToText(array $array) {
    return implode("\r\n", $array);
  }

  /**
   * {@inheritdoc}
   */
  public function addToQueue(\stdClass $data) {
    $this->queueFactory
      ->get('nth_mobile_psms_submit_message_worker')
      ->createItem($data);
  }

}
