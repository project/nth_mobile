<?php

namespace Drupal\nth_mobile_psms\Events;

use Drupal\Component\EventDispatcher\Event;

/**
 * Base class for all the NTH Mobile Premium SMS events.
 *
 * @package Drupal\nth_mobile_psms\Events
 */
abstract class NthMobilePsmsEventBase extends Event {

  /**
   * Context that initiated the action this event is triggered for.
   *
   * @var string
   */
  protected $context;

  /**
   * ID within the context corresponding to the NTH Mobile Premium SMS event.
   *
   * @var string
   */
  protected $contextId;

  /**
   * The data of NTH Mobile Premium SMS event.
   *
   * @var string[]
   */
  protected $data;

  /**
   * HTTP status code to return to NTH Mobile.
   *
   * @var int
   */
  protected $httpStatusCode;

  /**
   * NthMobilePsmsEventBase constructor.
   *
   * @param string $context
   *   Context that initiated the NTH Mobile Premium SMS action this event is
   *   triggered for.
   * @param string $contextId
   *   ID within the context corresponding to the NTH Mobile Premium SMS event.
   * @param array $data
   *   The data of NTH Mobile Premium SMS event.
   */
  public function __construct(string $context, string $contextId, array $data) {
    $this->context = $context;
    $this->contextId = $contextId;
    $this->data = $data;
  }

  /**
   * Returns the context for this event.
   *
   * @return string
   *   Context that initiated the NTH Mobile Premium SMS action this event is
   *   triggered for.
   */
  public function getContext(): string {
    return $this->context;
  }

  /**
   * Returns the context ID for this event.
   *
   * @return string
   *   ID within the context corresponding to the NTH Mobile Premium SMS event.
   */
  public function getContextId(): string {
    return $this->contextId;
  }

  /**
   * Returns the data of the NTH Mobile Premium SMS event.
   *
   * @return string[]
   *   Data of NTH Mobile Premium SMS event.
   */
  public function getData(): array {
    return $this->data;
  }

  /**
   * Sets the HTTP status code.
   *
   * @param int $httpStatusCode
   *   HTTP status code to return to NTH Mobile.
   */
  public function setHttpStatusCode(int $httpStatusCode): void {
    $this->httpStatusCode = $httpStatusCode;
  }

  /**
   * Returns the HTTP status code.
   *
   * @return int
   *   HTTP status code currently set to return to NTH Mobile.
   */
  public function getHttpStatusCode(): int {
    return $this->httpStatusCode;
  }

}
