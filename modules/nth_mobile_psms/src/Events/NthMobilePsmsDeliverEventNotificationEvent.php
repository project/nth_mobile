<?php

namespace Drupal\nth_mobile_psms\Events;

/**
 * Event notification for the NTH Mobile Premium SMS deliverEvent.
 *
 * @package Drupal\nth_mobile_psms\Events
 */
class NthMobilePsmsDeliverEventNotificationEvent extends NthMobilePsmsEventBase {

  const EVENT_NAME = 'nth_mobile_psms.notification_event.event';

  /**
   * Event types.
   */
  const OPTIN_MSG_SENT = 'optin_msg_sent';
  const OPTIN_CONFIRMED = 'optin_confirmed';
  const OPTIN_REFUSED = 'optin_refused';
  const OPTIN_TIMEDOUT = 'optin_timedout';
  const SERVICE_INFO_MSG_SENT = 'service_info_msg_sent';
  const SPENDING_INFO_MSG_SENT = 'spending_info_msg_sent';
  const SESSION_OPENED = 'session_opened';
  const SESSION_CLOSED = 'session_closed';
  const BROADCAST_MSG_SENT = 'broadcast_msg_sent';

  /**
   * The type of NTH Mobile Premium SMS event occurring.
   *
   * @var string
   */
  protected $eventType;

  /**
   * NthMobilePsmsDeliverEventNotificationEvent constructor.
   *
   * @param string $context
   *   Context that initiated the NTH Mobile Premium SMS action this event is
   *   triggered for.
   * @param string $contextId
   *   ID within the context corresponding to the NTH Mobile Premium SMS event.
   * @param array $data
   *   The data of NTH Mobile Premium SMS event.
   * @param string $eventType
   *   The type of event occurring.
   */
  public function __construct(string $context, string $contextId, array $data, string $eventType) {
    parent::__construct($context, $contextId, $data);

    $this->eventType = $eventType;
  }

  /**
   * Returns the type of event occurring.
   *
   * @return string
   *   The type of event occurring.
   */
  public function getEventType(): string {
    return $this->eventType;
  }

}
