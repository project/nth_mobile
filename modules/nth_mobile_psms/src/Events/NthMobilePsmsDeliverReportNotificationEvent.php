<?php

namespace Drupal\nth_mobile_psms\Events;

/**
 * Event notification for the NTH Mobile Premium SMS deliverReport.
 *
 * @package Drupal\nth_mobile_psms\Events
 */
class NthMobilePsmsDeliverReportNotificationEvent extends NthMobilePsmsEventBase {

  const EVENT_NAME = 'nth_mobile_psms.notification_event.report';

}
