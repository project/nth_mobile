<?php

namespace Drupal\nth_mobile_psms\Events;

/**
 * Event notification for the NTH Mobile Premium SMS deliverMessage.
 *
 * @package Drupal\nth_mobile_psms\Events
 */
class NthMobilePsmsDeliverMessageNotificationEvent extends NthMobilePsmsEventBase {

  const EVENT_NAME = 'nth_mobile_psms.notification_event.message';

}
