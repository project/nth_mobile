<?php

namespace Drupal\nth_mobile_psms\Entity;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\user\EntityOwnerInterface;
use Drupal\Core\Entity\EntityChangedInterface;

/**
 * Provides an interface defining a PSMS Transaction entity type.
 */
interface NthMobilePsmsTransactionInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Gets the PSMS Transaction title.
   *
   * @return string
   *   Title of the PSMS Transaction.
   */
  public function getTitle(): string;

  /**
   * Sets the PSMS Transaction title.
   *
   * @param string $title
   *   The PSMS Transaction title.
   *
   * @return $this
   *   The called PSMS Transaction entity.
   */
  public function setTitle(string $title);

  /**
   * Gets the PSMS Transaction creation timestamp.
   *
   * @return int
   *   Creation timestamp of the PSMS Transaction.
   */
  public function getCreatedTime(): int;

  /**
   * Sets the PSMS Transaction creation timestamp.
   *
   * @param int $timestamp
   *   The PSMS Transaction creation timestamp.
   *
   * @return $this
   *   The called PSMS Transaction entity.
   */
  public function setCreatedTime(int $timestamp);

  /**
   * Returns the PSMS Transaction status.
   *
   * @return bool
   *   TRUE if the PSMS Transaction is enabled, FALSE otherwise.
   */
  public function isEnabled(): bool;

  /**
   * Sets the PSMS Transaction status.
   *
   * @param bool $status
   *   TRUE to enable this PSMS Transaction, FALSE to disable.
   *
   * @return $this
   *   The called PSMS Transaction entity.
   */
  public function setStatus(bool $status);

  /**
   * Gets the PSMS Transaction message recipient's msisdn.
   *
   * @return string
   *   MSISDN of the PSMS Transaction's message.
   */
  public function getMsisdn(): string;

  /**
   * Gets the PSMS Transaction message business number.
   *
   * @return string
   *   Business number of the PSMS Transaction's message.
   */
  public function getBusinessNumber(): string;

  /**
   * Gets the PSMS Transaction message content.
   *
   * @return string
   *   Content of the PSMS Transaction's message.
   */
  public function getContent(): string;

  /**
   * Gets the PSMS Transaction message price.
   *
   * @return int
   *   Price of the PSMS Transaction's message.
   */
  public function getPrice(): int;

  /**
   * Gets the PSMS Transaction message session ID.
   *
   * @return string
   *   Session ID of the PSMS Transaction's message.
   */
  public function getSessionId(): string;

  /**
   * Gets the PSMS Transaction message operator code.
   *
   * @return string
   *   Operator code of the PSMS Transaction's message.
   */
  public function getOperatorCode(): string;

  /**
   * Gets the PSMS Transaction message ID.
   *
   * @return int
   *   Message ID of the PSMS Transaction's message.
   */
  public function getMessageId(): int;

  /**
   * Gets the PSMS Transaction message state.
   *
   * @return string
   *   Message state of the PSMS Transaction's message.
   */
  public function getMessageState(): string;

  /**
   * Gets the PSMS Transaction message type.
   *
   * @return string
   *   Message type of the PSMS Transaction's message.
   */
  public function getMessageType(): string;

}
