<?php

namespace Drupal\nth_mobile_psms\Entity;

use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\Core\StringTranslation\StringTranslationTrait;
use Drupal\user\UserInterface;

/**
 * Defines the PSMS Transaction entity class.
 *
 * @ContentEntityType(
 *   id = "nth_mobile_psms_transaction",
 *   label = @Translation("PSMS Transaction"),
 *   label_collection = @Translation("PSMS Transactions"),
 *   handlers = {
 *     "list_builder" = "Drupal\nth_mobile_psms\Controller\NthMobilePsmsTransactionListBuilder",
 *     "view_builder" = "Drupal\Core\Entity\EntityViewBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "form" = {
 *       "add" = "Drupal\nth_mobile_psms\Form\NthMobilePsmsTransactionForm",
 *       "edit" = "Drupal\nth_mobile_psms\Form\NthMobilePsmsTransactionForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm"
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     }
 *   },
 *   base_table = "nth_mobile_psms_transaction",
 *   revision_table = "nth_mobile_psms_transaction_revision",
 *   show_revision_ui = TRUE,
 *   admin_permission = "administer nth_mobile_psms transaction",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "label" = "title",
 *     "uuid" = "uuid"
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log"
 *   },
 *   links = {
 *     "add-form" = "/admin/nth-mobile/psms-transaction/add",
 *     "canonical" = "/psms-transaction/{nth_mobile_psms_transaction}",
 *     "edit-form" = "/admin/nth-mobile/psms-transaction/{nth_mobile_psms_transaction}/edit",
 *     "delete-form" = "/admin/nth-mobile/psms-transaction/{nth_mobile_psms_transaction}/delete",
 *     "collection" = "/admin/nth-mobile/psms-transaction"
 *   },
 *   field_ui_base_route = "entity.nth_mobile_psms_transaction.settings"
 * )
 */
class NthMobilePsmsTransaction extends RevisionableContentEntityBase implements NthMobilePsmsTransactionInterface {

  use EntityChangedTrait;
  use StringTranslationTrait;

  /**
   * {@inheritdoc}
   *
   * When a new PSMS Transaction entity is created, set the uid entity reference
   * to the current user as the creator of the entity.
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += ['uid' => \Drupal::currentUser()->id()];
  }

  /**
   * {@inheritdoc}
   */
  public function getTitle(): string {
    return $this->get('title')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setTitle($title) {
    $this->set('title', $title);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime(): int {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isEnabled(): bool {
    return (bool) $this->get('status')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setStatus($status) {
    $this->set('status', $status);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type) {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['title'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t('Title'))
      ->setDescription(t('The title of the PSMS Transaction entity.'))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'hidden',
        'type' => 'string',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDescription(t('A boolean indicating whether the PSMS Transaction is enabled.'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['description'] = BaseFieldDefinition::create('text_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Description'))
      ->setDescription(t('A description of the PSMS Transaction.'))
      ->setDisplayOptions('form', [
        'type' => 'text_textarea',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'text_default',
        'label' => 'above',
        'weight' => 10,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['msisdn'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t("MSISDN"))
      ->setDescription(t("Message recipient's msisdn, in international format with leading zeros (for example: 00414411112222). In some countries, due to end-user privacy protection rules, msisdn may be encrypted by mobile operator and as such forwarded in this parameter."))
      ->setRequired(TRUE)
      ->setSetting('max_length', 50)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['business_number'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t("Business number"))
      ->setDescription(t("Business number from which MT message was sent (originator)."))
      ->setRequired(TRUE)
      ->setSetting('max_length', 50)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['content'] = BaseFieldDefinition::create('string_long')
      ->setRevisionable(TRUE)
      ->setLabel(t('Content'))
      ->setDescription(t("Standard SMS message text may consist of up to 160 characters belonging to the Standard GSM character set.<br><br>If message text is using standard GSM characters and is longer than 160 characters then message will be split into parts (concatenated message) and as such sent to mobile user. If message text contains characters outside the standard GSM Character set, NTH will automatically recode it with 16-bit UCS2 character set, which means a maximum SMS length of 70 characters. If recoded message contains more then 70 characters, concatenation mechanism is applied.<br><br>For WAP Push messages this field must contain full URL of the content, for example: http://example.com/page.wml"))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textarea',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'basic_string',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['price'] = BaseFieldDefinition::create('integer')
      ->setRevisionable(TRUE)
      ->setLabel(t('Price'))
      ->setDescription(t('Message price in money units (provided by NTH).'))
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['session_id'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t("Session ID"))
      ->setDescription(t("Unique ID assigned to this particular session in NTH system. This is the value of the sessionId parameter received with MO message or returned in a response to initiation of web initiated service flow. This parameter is optional if there were no preceeding MO messages or if MT message being sent is initiating web intiated service flow."))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['message_id'] = BaseFieldDefinition::create('integer')
      ->setRevisionable(TRUE)
      ->setLabel(t("Message ID"))
      ->setDescription(t("Unique ID assigned to MT message in NTH system, returned in messageId field of submitMessage operation."))
      ->setSetting('size', 'big')
      ->setDisplayOptions('form', [
        'type' => 'number',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'number_integer',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['operator_code'] = BaseFieldDefinition::create('string')
      ->setRevisionable(TRUE)
      ->setLabel(t("Operator Code"))
      ->setDescription(t("Mobile number operator's network code (provided by NTH), used as MNO identifier, MT traffic routing and number portability between mobile operators. This parameter is mandatory in initial MT of Web initiated SMS service, and only in case if Number Lookup service is not enabled in Customer Account. In other words, it is mandatory if Customer is determining the routing to specific MNO."))
      ->setRequired(TRUE)
      ->setSetting('max_length', 255)
      ->setDisplayOptions('form', [
        'type' => 'string_textfield',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'string',
        'label' => 'above',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['message_state'] = BaseFieldDefinition::create('list_string')
      ->setRevisionable(TRUE)
      ->setLabel(t("Message state"))
      ->setDescription(t("The state of the transaction."))
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => [
          'draft' => t('Draft'),
          'pending' => t('Pending'),
          'delivered' => t('Delivered'),
          'failed' => t('Failed'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['message_type'] = BaseFieldDefinition::create('list_string')
      ->setRevisionable(TRUE)
      ->setLabel(t("Message type"))
      ->setDescription(t("The type of the transaction."))
      ->setRequired(TRUE)
      ->setSettings([
        'allowed_values' => [
          'mo' => t('MO'),
          'mt' => t('MT'),
        ],
      ])
      ->setDisplayOptions('form', [
        'type' => 'options_select',
        'weight' => -5,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'inline',
        'type' => 'list_default',
        'weight' => 9,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setLabel(t('Author'))
      ->setDescription(t('The user ID of the PSMS Transaction author.'))
      ->setSetting('target_type', 'user')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'author',
        'label' => 'above',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['action_executed'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Action executed'))
      ->setDescription(t('Whether or not the action for this message has been executed.'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Executed')
      ->setSetting('off_label', 'Not executed')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setDescription(t('The time that the PSMS Transaction was created.'))
      ->setDisplayOptions('view', [
        'type' => 'timestamp',
        'label' => 'above',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setDescription(t('The time that the PSMS Transaction was last edited.'));

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getMsisdn(): string {
    return $this->get('msisdn')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getBusinessNumber(): string {
    return $this->get('business_number')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getContent(): string {
    return $this->get('content')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getPrice(): int {
    return $this->get('price')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getSessionId(): string {
    return $this->get('session_id')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getOperatorCode(): string {
    return $this->get('operator_code')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageId(): int {
    return $this->get('message_id')->value ? $this->get('message_id')->value : 0;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageState(): string {
    return $this->get('message_state')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function getMessageType(): string {
    return $this->get('message_type')->value;
  }

}
