# NTH Mobile - Premium SMS

The NTH Mobile Premium SMS module enables Premium SMS payments in Drupal through
NTH Mobile. The module provides events that other modules can subscribe to and
build their business logic around.

## Configuration

### NTH Mobile Premium SMS account
For security reasons your NTH Mobile Premium SMS credentials cannot be managed
through the Drupal UI. Add your NTH Mobile Premium SMS credentials to the
settings.php file:

```php
$settings['nth_mobile_psms.settings'] = [
  'username' => 'user1',
  'password' => 'pass1',
];
```

### Queue worker
There is a queue worker to process not submitted MT messages. You should call
the queue worker from drush with cron to retry sending the MT messages. Default
cron is disabled for this QueueWorker.

Example to retry sending every 5 minutes:
```shell
*/5 * * * * drush queue:run nth_mobile_psms_submit_message_worker
```

## Developer documentation

See [developer documentation](docs/DOCUMENTATION.md).
