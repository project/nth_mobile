# Developer Documentation

https://developers.nth-mobile.com/premium-sms

## Operations
This sections describes all operations available in Premium SMS HTTP API.

### Send (single) (text) MT message

**Request**
```http request
POST /nth-mobile/premium-sms/premium HTTP/1.1

Host: example.com
Content type: application/x-www-form-urlencoded

{
  "command": "submitMessage",
  "username": "user1",
  "password": "pass1",
  "msisdn": "00414411112222",
  "businessNumber": "9292",
  "content": "MT+message+text",
  "price": "100",
  "sessionId": "9292CHA1571000000000"
}
```

**Response**
```xml
<res>
  <resultCode>100</resultCode>
  <resultText>OK</resultText>
  <messageId>12345</messageId>
  <messageRef>CUST_REF_12345</messageRef>
  <sessionId>9292CHA1571000000000</sessionId>
  <operatorCode>22801</operatorCode>
</res>
```

**Callback**

See [MT SMS delivery reports](#mt-sms-delivery-reports)

## MO SMS Delivery & Callbacks
This sections describes all callbacks and operations directed from SMS Gateway
towards the Customer, available in Premium SMS HTTP API.

### MO SMS delivery

**Request**
```http request
POST /nth-mobile/premium-sms/message HTTP/1.1

Host: example.com
Content type: application/x-www-form-urlencoded

{
  "command": "deliverMessage",
  "messageId": "12345",
  "msisdn": "00414411112222",
  "businessNumber": "9292",
  "keyword": "START",
  "content": "START+sms+service",
  "operatorCode": "22801",
  "sessionId": "9292CHA1571000000000",
  "time": "2021-01-01+12%3A00%3A00"
}
```

**Response**
```http request
HTTP/1.1 200 OK
```

### MT SMS delivery reports

**Request**
```http request
POST /nth-mobile/premium-sms/report HTTP/1.1

Host: example.com
Content type: application/x-www-form-urlencoded

{
  "command": "deliverReport",
  "messageId": "12345",
  "messageRef": "CUST_REF_12345",
  "msisdn": "00414411112222",
  "businessNumber": "9292",
  "messageStatus": "2",
  "messageStatusText": "Delivery+successful",
  "time": "2021-01-01+12%3A00%3A00",
  "sessionId": "9292CHA1571000000000"
}
```

**Response**
```http request
HTTP/1.1 200 OK
```

### Event notifications

**Request**
```http request
POST /nth-mobile/premium-sms/event HTTP/1.1

Host: example.com
Content type: application/x-www-form-urlencoded

{
  "command" :"deliverEvent",
  "event": "optin_msg_sent",
  "msisdn": "00414411112222",
  "businessNumber": "9292",
  "operatorCode": "22801",
  "keyword": "GAME",
  "sessionId": "9292CHA1571000000000",
  "time": "2021-01-01+12%3A00%3A00",
  "price": "0",
  "content": "Reply+with+YES+to+confirm+purchase",
  "messageId": "12345"
}
```

**Response**
```http request
HTTP/1.1 200 OK
```
