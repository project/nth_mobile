<?php

namespace Drupal\nth_mobile_psms_endpoint_test\Controller;

use Drupal\Core\Controller\ControllerBase;
use Laminas\Diactoros\Response\XmlResponse;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Symfony\Component\HttpFoundation\RequestStack;

/**
 * Returns responses for NTH Mobile endpoint test routes.
 */
class WebhookController extends ControllerBase {

  /**
   * The request stack.
   *
   * @var \Symfony\Component\HttpFoundation\RequestStack
   */
  protected $requestStack;

  /**
   * WebhookController constructor.
   *
   * @param \Symfony\Component\HttpFoundation\RequestStack $request_stack
   *   The request stack.
   */
  public function __construct(RequestStack $request_stack) {
    $this->requestStack = $request_stack;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    return new static(
      $container->get('request_stack')
    );
  }

  /**
   * This webhook is only for testing purposes.
   *
   * @return \Laminas\Diactoros\Response\XmlResponse
   *   Response that will inform you whether the submission was processed
   *   correctly.
   */
  public function invokeSubmitMessageHook(): XmlResponse {
    $params = $this->getParameters();

    $domtree = new \DOMDocument('1.0', 'UTF-8');
    $res = $domtree->createElement("res");
    $domtree->appendChild($res);

    $res->appendChild($domtree->createElement('resultCode', '100'));
    $res->appendChild($domtree->createElement('resultText', 'OK'));
    $res->appendChild($domtree->createElement('messageId', '12345'));
    $res->appendChild($domtree->createElement('messageRef', $params['messageRef']));
    $res->appendChild($domtree->createElement('sessionId', $params['sessionId']));
    $res->appendChild($domtree->createElement('operatorCode', '22801'));

    return new XmlResponse($domtree->saveXML());
  }

  /**
   * This webhook is only for testing purposes.
   *
   * @return \Laminas\Diactoros\Response\XmlResponse
   *   Response that will inform you whether the submission was processed
   *   correctly.
   */
  public function invokeFailedSubmitMessageHook(): XmlResponse {
    $params = $this->getParameters();

    $domtree = new \DOMDocument('1.0', 'UTF-8');
    $xmlRoot = $domtree->createElement("xml");
    $xmlRoot = $domtree->appendChild($xmlRoot);

    $res = $domtree->createElement("res");
    $res = $xmlRoot->appendChild($res);
    $res->appendChild($domtree->createElement('resultCode', '203'));
    $res->appendChild($domtree->createElement('resultText', 'Source address (originator) blacklisted'));
    $res->appendChild($domtree->createElement('messageId', '12345'));
    $res->appendChild($domtree->createElement('messageRef', $params['messageRef']));
    $res->appendChild($domtree->createElement('sessionId', $params['sessionId']));
    $res->appendChild($domtree->createElement('operatorCode', '22801'));

    return new XmlResponse($domtree->saveXML());
  }

  /**
   * Get the parameters from the request.
   *
   * @return string[]
   *   The parameters.
   */
  protected function getParameters() {
    return [
      'sessionId' => $this->requestStack->getCurrentRequest()->get('sessionId'),
      'messageRef' => $this->requestStack->getCurrentRequest()->get('messageRef'),
    ];
  }

}
