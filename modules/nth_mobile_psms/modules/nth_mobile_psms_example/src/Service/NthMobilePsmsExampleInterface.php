<?php

namespace Drupal\nth_mobile_psms_example\Service;

/**
 * Interface for NthMobilePsmsExample.
 *
 * Describes the NthMobilePsmsExample service functions.
 */
interface NthMobilePsmsExampleInterface {

  /**
   * Get the NTH mobile Premium SMS example logger channel.
   *
   * @return \Psr\Log\LoggerInterface
   *   The logger.
   */
  public function getLogger();

}
