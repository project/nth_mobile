<?php

namespace Drupal\nth_mobile_psms_example\Service;

use Psr\Log\LoggerInterface;

/**
 * Class NthMobilePsmsExample.
 *
 * The NthMobilePsmsExample service.
 */
class NthMobilePsmsExample implements NthMobilePsmsExampleInterface {

  /**
   * The logger.
   *
   * @var \Psr\Log\LoggerInterface
   */
  protected $logger;

  /**
   * Constructs a NthMobilePsmsExample object.
   *
   * @param \Psr\Log\LoggerInterface $logger
   *   The logger interface.
   */
  public function __construct(LoggerInterface $logger) {
    $this->logger = $logger;
  }

  /**
   * {@inheritdoc}
   */
  public function getLogger() {
    return $this->logger;
  }

}
