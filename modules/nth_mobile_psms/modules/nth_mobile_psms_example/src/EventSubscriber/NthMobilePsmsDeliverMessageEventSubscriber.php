<?php

namespace Drupal\nth_mobile_psms_example\EventSubscriber;

use Drupal\Component\Utility\Html;
use Drupal\Core\Site\Settings;
use Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverMessageNotificationEvent;
use Hashids\Hashids;

/**
 * Event subscriber for the NthMobilePsmsDeliverMessageNotificationEvent.
 *
 * @package Drupal\nth_mobile_psms_example\EventSubscriber
 */
class NthMobilePsmsDeliverMessageEventSubscriber extends NthMobilePsmsEventSubscriberBase {

  /**
   * You need to implement the deliverMessage logic here.
   *
   * @param \Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverMessageNotificationEvent $event
   *   Event.
   */
  public function deliverMessage(NthMobilePsmsDeliverMessageNotificationEvent $event): void {
    // Return if the event context is not this module.
    if ($event->getContext() !== 'premium_sms') {
      return;
    }

    try {
      $this->nthMobilePsmsExample->getLogger()
        ->debug('You need to implement the deliverMessage logic here.');

      // Construct MT message.
      $mt_message = new \stdClass();
      $mt_message->data = $event->getData();
      $mt_message->language = 'en';

      // For security reasons we created a hash from the user ID with another
      // number and we extract the user ID from the hash as first param.
      //
      // Content:
      // 0. Keyword
      // 1. Type
      // 2. Hash.
      $content = explode('+', $mt_message->data['content']);
      $hashIds = new Hashids(Settings::getHashSalt());
      $decoded_hash = $hashIds->decode(Html::escape($content[2]));
      $mt_message->userId = intval($decoded_hash[0]);

      // Free MT Message.
      $mt_message->content = 'This is your Free MT message content.';
      $mt_message->price = 0;

      // Premium MT message.
      if ($this->nthMobilePsms->isMtOperator($mt_message->data['operatorCode'])) {
        $mt_message->content = 'This is your Premium MT message content.';
        $mt_message->price = 100;
      }

      try {
        // Send MT Message.
        $result = $this->nthMobilePsmsApi->submitMessage(
          $mt_message->content,
          $mt_message->data,
          $mt_message->price,
          $mt_message->userId,
          $mt_message->language
        );

        // Add MT Message to queue when message could not be sent or something
        // was wrong in the result.
        if (!$this->nthMobilePsmsApi->processResultCode($result)) {
          if (isset($result['retryMessageRef'])) {
            $mt_message->retryMessageRef = $result['retryMessageRef'];
          }
          $this->nthMobilePsms->addToQueue($mt_message);
        }
      }
      catch (\Exception $e) {
        watchdog_exception('nth_mobile_psms_example', $e);
      }

      $httpStatusCode = 200;
    }
    catch (\Exception $e) {
      watchdog_exception('nth_mobile_psms_example', $e);
      $httpStatusCode = 500;
    }

    // Set the HTTP code to return to NTH Mobile.
    $event->setHttpStatusCode($httpStatusCode);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      NthMobilePsmsDeliverMessageNotificationEvent::EVENT_NAME => 'deliverMessage',
    ];
  }

}
