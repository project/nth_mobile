<?php

namespace Drupal\nth_mobile_psms_example\EventSubscriber;

use Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverEventNotificationEvent;

/**
 * Event subscriber for the NthMobilePsmsDeliverEventNotificationEvent.
 *
 * @package Drupal\nth_mobile_psms_example\EventSubscriber
 */
class NthMobilePsmsDeliverEventEventSubscriber extends NthMobilePsmsEventSubscriberBase {

  /**
   * You need to implement the deliverEvent logic here.
   *
   * @param \Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverEventNotificationEvent $event
   *   Event.
   */
  public function deliverEvent(NthMobilePsmsDeliverEventNotificationEvent $event): void {
    // Return if the event context is not this module.
    if ($event->getContext() !== 'premium_sms') {
      return;
    }

    try {
      $this->nthMobilePsmsExample->getLogger()->debug('You need to implement the deliverEvent logic here.');
      $httpStatusCode = 200;
    }
    catch (\Exception $e) {
      watchdog_exception('nth_mobile_psms_example', $e);
      $httpStatusCode = 500;
    }

    // Set the HTTP code to return to NTH Mobile.
    $event->setHttpStatusCode($httpStatusCode);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      NthMobilePsmsDeliverEventNotificationEvent::EVENT_NAME => 'deliverEvent',
    ];
  }

}
