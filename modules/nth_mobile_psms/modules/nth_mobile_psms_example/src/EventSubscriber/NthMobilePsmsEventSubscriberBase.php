<?php

namespace Drupal\nth_mobile_psms_example\EventSubscriber;

use Drupal\nth_mobile_psms\Service\NthMobilePsmsApiInterface;
use Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface;
use Drupal\nth_mobile_psms_example\Service\NthMobilePsmsExampleInterface;
use Symfony\Component\EventDispatcher\EventSubscriberInterface;
use Symfony\Component\HttpKernel\HttpKernelInterface;

/**
 * Event subscriber base for the NthMobilePsmsNotificationEvents.
 *
 * @package Drupal\nth_mobile_psms_example\EventSubscriber
 */
abstract class NthMobilePsmsEventSubscriberBase implements EventSubscriberInterface {

  /**
   * HTTP kernel.
   *
   * @var \Symfony\Component\HttpKernel\HttpKernelInterface
   */
  protected $httpKernel;

  /**
   * The NthMobilePsms service.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface
   */
  protected $nthMobilePsms;

  /**
   * The NthMobilePsmsApi service.
   *
   * @var \Drupal\nth_mobile_psms\Service\NthMobilePsmsApiInterface
   */
  protected $nthMobilePsmsApi;

  /**
   * The NthMobilePsmsExample service.
   *
   * @var \Drupal\nth_mobile_psms_example\Service\NthMobilePsmsExampleInterface
   */
  protected $nthMobilePsmsExample;

  /**
   * NthMobilePsmsEventSubscriberBase constructor.
   *
   * @param \Symfony\Component\HttpKernel\HttpKernelInterface $httpKernel
   *   HTTP kernel.
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsInterface $nthMobilePsms
   *   The NthMobilePsms service.
   * @param \Drupal\nth_mobile_psms\Service\NthMobilePsmsApiInterface $nthMobilePsmsApi
   *   The NthMobilePsmsApi service.
   * @param \Drupal\nth_mobile_psms_example\Service\NthMobilePsmsExampleInterface $nthMobilePsmsExample
   *   The NthMobilePsmsExample service.
   */
  public function __construct(HttpKernelInterface $httpKernel, NthMobilePsmsInterface $nthMobilePsms, NthMobilePsmsApiInterface $nthMobilePsmsApi, NthMobilePsmsExampleInterface $nthMobilePsmsExample) {
    $this->httpKernel = $httpKernel;
    $this->nthMobilePsms = $nthMobilePsms;
    $this->nthMobilePsmsApi = $nthMobilePsmsApi;
    $this->nthMobilePsmsExample = $nthMobilePsmsExample;
  }

}
