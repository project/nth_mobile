<?php

namespace Drupal\nth_mobile_psms_example\EventSubscriber;

use Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverReportNotificationEvent;

/**
 * Event subscriber for the NthMobilePsmsDeliverReportEvent.
 *
 * @package Drupal\nth_mobile_psms_example\EventSubscriber
 */
class NthMobilePsmsDeliverReportEventSubscriber extends NthMobilePsmsEventSubscriberBase {

  /**
   * You need to implement the deliverReport logic here.
   *
   * @param \Drupal\nth_mobile_psms\Events\NthMobilePsmsDeliverReportNotificationEvent $event
   *   Event.
   */
  public function deliverReport(NthMobilePsmsDeliverReportNotificationEvent $event): void {
    // Return if the event context is not this module.
    if ($event->getContext() !== 'premium_sms') {
      return;
    }

    try {
      $this->nthMobilePsmsExample->getLogger()->debug('You need to implement the deliverReport logic here.');
      $httpStatusCode = 200;
    }
    catch (\Exception $e) {
      watchdog_exception('nth_mobile_psms_example', $e);
      $httpStatusCode = 500;
    }

    // Set the HTTP code to return to NTH Mobile.
    $event->setHttpStatusCode($httpStatusCode);
  }

  /**
   * {@inheritdoc}
   */
  public static function getSubscribedEvents(): array {
    return [
      NthMobilePsmsDeliverReportNotificationEvent::EVENT_NAME => 'deliverReport',
    ];
  }

}
